# -*- coding: utf-8 -*-
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import HTMLConverter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from cStringIO import StringIO
import glob
import re
import shutil
import csv
import lxml
from lxml import etree
from lxml import html
from datetime import date, timedelta
import datetime
import os
import sys
import getopt

def convert_pdf_to_html(argv):
    
    pdfPath = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'pdf')
    
    try:
        opts, args = getopt.getopt(argv,"hc:",['config='])
        for opt, arg in opts:
            if opt == '-h':
                print 'konwerter.py -s <pdf source folder>'
                sys.exit()
            elif opt in ("-s", "--source"):
                pdfPath = arg
    except getopt.GetoptError:
        print 'konwerter.py -c <pdf source folder>'
        sys.exit(2)
    
    files = glob.glob(os.path.join(pdfPath, "*.pdf"))
    
    csvPath = os.path.join(pdfPath, 'csv')
    if not os.path.exists(csvPath):
        os.makedirs(csvPath)
    
    donePath = os.path.join(pdfPath, 'done')
    if not os.path.exists(donePath):
        os.makedirs(donePath)
    
    #stworzenie pliku csv i dodanie nagłówka.
    today_string = datetime.datetime.today().strftime('%Y%m%d%H%M%S')
    namefile = os.path.join(csvPath,'%s.csv') % (today_string)
    text_file = open(namefile, "a")
    header = ("%s,%s,%s,%s,%s,%s,%s,%s\n")%(('Numer identyfikacyjny klienta'),('Numer zlecenia'),
                                         ("Nazwa"),('Ulica'),('Miasto'),
                                          (("Tel. Komorkowy").encode('utf-8')),
                                          (("Tel. Sluzbowy").encode('utf-8')),
                                          ('nazwa pliku'))
    text_file.write(header)
    
    for file in files:
        lines = get_lines(file)
        for line in lines:
            text_file.write(line)
        text_file.flush()
        shutil.move(file, donePath)
        
    text_file.close()
    
def get_lines(pdfPath):

    lines = []
    
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    laparams = LAParams()
    device = HTMLConverter(rsrcmgr, retstr, codec='utf-8', laparams=laparams)
    fp = file(pdfPath, 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    maxpages = 0 #is for all
    caching = True
    pagenos=set()
    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password,caching=caching, check_extractable=True):
        interpreter.process_page(page)
    str = retstr.getvalue()
    root = html.document_fromstring(str)
    e = root.xpath('.//span[starts-with(text(),"Zlecenie ")]')
    
    zlecenia = []
    
    for zlecenie in e:
        div17 = zlecenie.getparent() 
        div16 = div17.getprevious()   
        div15 = div16.getprevious()
        div14 = div15.getprevious()
        div13 = div14.getprevious()
        div12 = div13.getprevious()
        div11 = div12.getprevious()
        div10 = div11.getprevious()
        div9 = div10.getprevious()
        div8 = div9.getprevious()
        div7 = div8.getprevious()
        div6 = div7.getprevious()
        div5 = div6.getprevious()
        div4 = div5.getprevious()
        div3 = div4.getprevious()
        div2 = div3.getprevious()
        div18 = div17.getnext()
        div19 = div18.getnext()
        div20 = div19.getnext()
        div21 = div20.getnext()
        div22 = div21.getnext()
        div23 = div22.getnext()
        div24 = div23.getnext()
        div25 = div24.getnext()
        div26 = div25.getnext()
        div27 = div26.getnext()
        div28 = div27.getnext()
        div29 = div28.getnext()
        div30 = div29.getnext()


        nazwa = ''
        ulica = ''
        miejscowosc = ''
        klient_id = ''
        nr_zlecenia = ''
        tel_kom = ''
        tel_slu = ''
        
        if len(div25.getchildren()) > 0:
            nazwa = div25.getchildren()[0].text
        if len(div15.getchildren()) > 0:
            ulica = div15.getchildren()[0].text
        if len(div18.getchildren()) > 0:
            miejscowosc = div18.getchildren()[0].text
        if len(div30.getchildren()) > 0:
            klient_id = div30.getchildren()[0].text
        if len(div21.getchildren()) > 0:
            nr_zlecenia = div21.getchildren()[0].text
        if len(div4.getchildren()) > 0:
            tel_kom = div4.getchildren()[0].text
        if len(div2.getchildren()) > 0:
            tel_slu = div2.getchildren()[0].text
        
        wiersz = ("%s,%s,%s,%s,%s,%s,%s,%s\n")%((klient_id.encode('utf-8')), 
                                            (nr_zlecenia.encode('utf-8')), (nazwa.encode('utf-8')), 
                                            (ulica.encode('utf-8')),                                                   
                                            (miejscowosc.encode('utf-8')),
                                            (tel_kom.encode('utf-8')),
                                            (tel_slu.encode('utf-8')),
                                            pdfPath,
                                            )
        lines.append(wiersz)        
        
    fp.close()
    device.close()
    retstr.close()    
    
    return lines
if __name__ == '__main__':
    convert_pdf_to_html(sys.argv[1:])
